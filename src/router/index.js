import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Lab from '@/components/Lab'
import Protocol from '@/components/Protocol'
import Update from '@/components/Update'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/lab',
      name: 'Lab',
      component: Lab
    },
    {
      path: '/protocol',
      name: 'Protocol',
      component: Protocol
    },
    {
      path: '/update',
      name: 'Update',
      component: Update
    }
  ]
})
